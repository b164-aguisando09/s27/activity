const http = require("http");

const port = 4000;

let directory = [
	{
		"name": "Mary Jane",
		"lastName": "Dela Cruz",
		"mobileNumber": "09123456789",
		"email": "mjdelacruz@gmail.com",
		"password": 123
	},
	{
		"name": "John",
		"lastName": "Doe",
		"mobileNumber": "09123456789",
		"email": "jdoe@gmail.com",
		"password": 123
	}

]

const server = http.createServer((req, res) => {

	if (req.url == "/profile" && req.method == "GET") {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(directory));
		res.end();
	}


	if (req.url == "/profile" && req.method == "POST") {

		let requestBody = "";
		req.on('data', function(data){
			requestBody += data;
		});

		req.on('end', function(){

			console.log(typeof requestBody);

			requestBody = JSON.parse(requestBody);

			let newProfile = {
				"name": requestBody.name,
				"lastName": requestBody.lastName,
				"mobileNumber": requestBody.mobileNumber,
				"email": requestBody.email,
				"password": requestBody.password
			};

			directory.push(newProfile);
			console.log(directory);
	
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(newProfile));
		res.end();

		})
	}
})

server.listen(port);

console.log(`Server is running at localhost: ${port}`);